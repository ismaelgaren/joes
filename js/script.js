function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  
  function validate(e) {
      e.preventDefault();
    const email = $("#userEmail").val();
  
    if (validateEmail(email)) {
        Swal.fire(
            'Great!',
            'You are now subscribed to our newsletter! Kindly check your email for updates.',
            'success'
        )
    } else if(email === "") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<p>Please enter email.</p>'
        })
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<p>Please enter valid email.</p>'
        })
    }
    return false;
  }
  
  $("#subscribeBtn").on("click", validate);

function sendMsg(e){
    e.preventDefault();

    let fullName = document.getElementById('fullName').value;
    let userEmail = document.getElementById('userEmail').value;
    let bodyMsg = document.getElementById('bodyMsg').value;

    if(fullName && userEmail && bodyMsg !== ""){
        Swal.fire(
            'Great!',
            'Thank you for messaging us!',
            'success'
        )
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<p>Please check credentials.</p>'
        })
    }
}
let btn = document.querySelector('#msgBtn');
btn.addEventListener('click', sendMsg)
